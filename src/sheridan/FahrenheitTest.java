package sheridan;

import static org.junit.Assert.*;
import org.junit.Test;

public class FahrenheitTest {

	@Test
	public void testFromCelsiusRegular() {
		boolean result = Fahrenheit.fromCelsius(5) >= 0;
		assertTrue("Celsius not converted to Fahrenheit properly", result);
	}
	
	@Test
	public void testFromCelsiusException() {
		boolean result = Fahrenheit.fromCelsius(-100) >= 0;
		assertFalse("Celsius not converted to Fahrenheit properly", result);
	}
	
	@Test
	public void testFromCelsiusBoundaryIn() {
		boolean result = Fahrenheit.fromCelsius(-32) >= 0;
		assertTrue("Celsius not converted to Fahrenheit properly", result);
	}
	
	@Test
	public void testFromCelsiusBoundaryOut() {
		boolean result = Fahrenheit.fromCelsius(-33) >= 0;
		assertFalse("Celsius not converted to Fahrenheit properly", result);
	}

}
